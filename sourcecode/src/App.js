import "./App.css";
import ApplicationBar from "./components/ApplicationBar/ApplicationBar";
import { Routes, Route, useLocation } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import Form from "./Pages/FormPage/Form";
import { AnimatePresence } from "framer-motion";
import Maps from "./Pages/MapsPage/Maps";

function App() {
  const location = useLocation();
  return (
    <div>
      <ApplicationBar />
      <AnimatePresence>
        <Routes location={location} key={location.pathname}>
          <Route path="/" element={<HomePage />} />
          <Route path="/form" element={<Form />} />
          <Route path="/map" element={<Maps />} />
        </Routes>
      </AnimatePresence>
    </div>
  );
}

export default App;
