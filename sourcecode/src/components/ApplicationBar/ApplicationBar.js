import React from "react";
import { Link } from "react-router-dom";
import "./ApplicationBar.css";

const ApplicationBar = () => {
  return (
    <div className="appbar-content">
      <div className="logo">
        <Link to="/" className="logo-link">
          GlobalExplorer
        </Link>
      </div>
      <div className="navigation-links">
        <div className="navigation-item">
          <Link className="nav-link" to="/">
            HOME
          </Link>
        </div>
        <div className="navigation-item">
          <Link className="nav-link" to="/form">
            FORM
          </Link>
        </div>
        <div className="navigation-item">
          <Link className="nav-link" to="/map">
            MAP
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ApplicationBar;
