import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import "./Form.css";
import Transition from "../../components/Utils/Transition";

const Form = () => {
  const [formData, setFormData] = useState({
    destination: "",
    travelDates: "",
    travelDates1: "",
    preferences: "",
    budget: "",
  });
  const [response, setResponse] = useState("");
  const [errors, setErrors] = useState({});

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const validateForm = () => {
    let tempErrors = {};
    let isValid = true;

    if (!formData.destination) {
      isValid = false;
      tempErrors["destination"] = "Acest câmp este obligatoriu.";
    }
    if (!formData.travelDates) {
      isValid = false;
      tempErrors["travelDates"] = "Acest câmp este obligatoriu.";
    }
    if (!formData.travelDates1) {
      isValid = false;
      tempErrors["travelDates1"] = "Acest câmp este obligatoriu.";
    }
    if (!formData.preferences) {
      isValid = false;
      tempErrors["preferences"] = "Acest câmp este obligatoriu.";
    }
    if (!formData.budget) {
      isValid = false;
      tempErrors["budget"] = "Acest câmp este obligatoriu.";
    }

    setErrors(tempErrors);
    return isValid;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!validateForm()) {
      return;
    }

    const userPrompt = `Vreau să-mi organizezi o călătorie. Planific o călătorie către ${formData.destination} începând de la ${formData.travelDates1} până la ${formData.travelDates}. Preferințele mele sunt: ${formData.preferences}. Poți să îmi dai câteva sfaturi?`;

    try {
      const res = await fetch("http://localhost:3000/getResponse", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ userPrompt }),
      });

      const data = await res.text();
      setResponse(data);
    } catch (error) {
      console.error("Error fetching response:", error);
      setResponse("A intervenit o eroare în obținerea răspunsului.");
    }
  };

  return (
    <div className="form-container">
      <h6 className="form-title">Completare formular:</h6>
      <form onSubmit={handleSubmit}>
        <div className="form-row">
          <div className="form-field">
            <TextField
              label="Destinație"
              name="destination"
              value={formData.destination}
              onChange={handleChange}
              variant="filled"
              InputLabelProps={{ style: { color: "black" } }}
              style={{ backgroundColor: "#fff" }}
              error={!!errors.destination}
              helperText={errors.destination}
            />
          </div>
          <div className="form-field">
            <TextField
              label="Data sosire"
              name="travelDates"
              value={formData.travelDates}
              onChange={handleChange}
              variant="filled"
              InputLabelProps={{ style: { color: "black" } }}
              style={{ backgroundColor: "#fff" }}
              error={!!errors.travelDates}
              helperText={errors.travelDates}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-field">
            <TextField
              label="Data plecare"
              name="travelDates1"
              value={formData.travelDates1}
              onChange={handleChange}
              variant="filled"
              InputLabelProps={{ style: { color: "black" } }}
              style={{ backgroundColor: "#fff" }}
              error={!!errors.travelDates1}
              helperText={errors.travelDates1}
            />
          </div>
          <div className="form-field">
            <TextField
              label="Atracții turistice preferate"
              name="preferences"
              value={formData.preferences}
              onChange={handleChange}
              variant="filled"
              InputLabelProps={{ style: { color: "black" } }}
              style={{ backgroundColor: "#fff" }}
              error={!!errors.preferences}
              helperText={errors.preferences}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-field">
            <TextField
              label="Buget"
              name="budget"
              value={formData.budget}
              onChange={handleChange}
              variant="filled"
              InputLabelProps={{ style: { color: "black" } }}
              style={{ backgroundColor: "#fff" }}
              error={!!errors.budget}
              helperText={errors.budget}
            />
          </div>
        </div>
        <button className="submit-button" type="submit">
          Trimite
        </button>
      </form>
      {response && (
        <div className="response-container">
          <h2>Răspuns:</h2>
          <p>{response}</p>
        </div>
      )}
    </div>
  );
};

export default Transition(Form);
