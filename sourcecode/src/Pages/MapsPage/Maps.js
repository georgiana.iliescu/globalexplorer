import React, { useState } from "react";
import {
  GoogleMap,
  useJsApiLoader,
  DirectionsRenderer,
  InfoWindow,
  Marker,
} from "@react-google-maps/api";
import TextField from "@mui/material/TextField";

const containerStyle = {
  width: "1680px",
  height: "700px",
  margin: "0 auto",
};

const defaultCenter = {
  lat: -3.745,
  lng: -38.523,
};

const buttonStyle = {
  padding: "10px 15px",
  border: "none",
  borderRadius: "5px",
  background: "#000",
  color: "#fff",
  fontSize: "16px",
  cursor: "pointer",
  marginTop: "5px",
  textAlign: "center",
};

const buttonHoverStyle = {
  background: "#333",
};

function Maps() {
  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: "AIzaSyB1-GgcKfs-rBJ7yHpfZQi2qi_tQLGTlyc",
    libraries: ["places"],
  });

  const [map, setMap] = useState(null);
  const [directions, setDirections] = useState(null);
  const [start, setStart] = useState("");
  const [destination, setDestination] = useState("");
  const [infoPosition, setInfoPosition] = useState(null);
  const [infoContent, setInfoContent] = useState("");
  const [search, setSearch] = useState("");
  const [attractionMarkers, setAttractionMarkers] = useState([]);
  const [selectedAttraction, setSelectedAttraction] = useState(null);

  const onLoad = React.useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds(defaultCenter);
    map.fitBounds(bounds);
    setMap(map);
  }, []);

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null);
  }, []);

  const resetDirections = () => {
    setDirections(null);
    setInfoPosition(null);
    setInfoContent("");
    setAttractionMarkers([]);
  };

  const handleDirections = () => {
    if (start && destination) {
      resetDirections();

      const directionsService = new window.google.maps.DirectionsService();
      directionsService.route(
        {
          origin: start,
          destination: destination,
          travelMode: "TRANSIT",
        },
        (result, status) => {
          if (status === "OK") {
            setDirections(result);
            const distanceInMeters = result.routes[0].legs[0].distance.value;
            const distanceInKm = distanceInMeters / 1000;
            setInfoContent(distanceInKm.toFixed(2) + " km");
            const endLocation = result.routes[0].legs[0].end_location;
            setInfoPosition(endLocation);

            const placesService = new window.google.maps.places.PlacesService(
              map
            );
            result.routes[0].legs[0].steps.forEach((step) => {
              const request = {
                location: step.end_location,
                radius: "50000",
                type: ["airport"],
              };
              placesService.nearbySearch(request, (results, status) => {
                if (
                  status === window.google.maps.places.PlacesServiceStatus.OK
                ) {
                  const markers = results.map((place) => ({
                    position: place.geometry.location,
                    name: place.name,
                  }));
                }
              });
            });

            const bounds = new window.google.maps.LatLngBounds();
            result.routes[0].overview_path.forEach((point) => {
              bounds.extend(point);
            });
            map.fitBounds(bounds);
          } else {
            console.error(`Error fetching directions ${result}`);
          }
        }
      );
    }
  };

  const handleSearch = () => {
    const geocoder = new window.google.maps.Geocoder();
    geocoder.geocode({ address: search }, (results, status) => {
      if (status === "OK" && results[0]) {
        const location = results[0].geometry.location;
        map.panTo(location);
        map.setZoom(12);

        const placesService = new window.google.maps.places.PlacesService(map);
        const request = {
          location: location,
          radius: "5000",
          type: ["tourist_attraction"],
        };
        placesService.nearbySearch(request, (results, status) => {
          if (status === window.google.maps.places.PlacesServiceStatus.OK) {
            const markers = results.map((place) => ({
              position: place.geometry.location,
              name: place.name,
            }));
            setAttractionMarkers(markers);
          }
        });
      } else {
        console.error("Error finding location");
      }
    });
  };

  return isLoaded ? (
    <div style={{ textAlign: "center" }}>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginBottom: "10px",
        }}
      >
        <TextField
          label="Start address"
          name="start"
          value={start}
          onChange={(e) => setStart(e.target.value)}
          variant="filled"
          InputLabelProps={{ style: { color: "black" } }}
          style={{ backgroundColor: "#fff", marginRight: "10px" }}
        />
        <TextField
          label="Destination address"
          name="destination"
          value={destination}
          onChange={(e) => setDestination(e.target.value)}
          variant="filled"
          InputLabelProps={{ style: { color: "black" } }}
          style={{ backgroundColor: "#fff", marginRight: "10px" }}
        />
        <button
          onClick={handleDirections}
          style={buttonStyle}
          onMouseOver={(e) =>
            (e.currentTarget.style.background = buttonHoverStyle.background)
          }
          onMouseOut={(e) =>
            (e.currentTarget.style.background = buttonStyle.background)
          }
        >
          Get Directions
        </button>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginBottom: "10px",
        }}
      >
        <TextField
          label="Search location"
          name="search"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          variant="filled"
          InputLabelProps={{ style: { color: "black" } }}
          style={{ backgroundColor: "#fff", marginRight: "10px" }}
        />
        <button
          onClick={handleSearch}
          style={buttonStyle}
          onMouseOver={(e) =>
            (e.currentTarget.style.background = buttonHoverStyle.background)
          }
          onMouseOut={(e) =>
            (e.currentTarget.style.background = buttonStyle.background)
          }
        >
          Search
        </button>
      </div>
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={defaultCenter}
        zoom={10}
        onLoad={onLoad}
        onUnmount={onUnmount}
      >
        {directions && <DirectionsRenderer directions={directions} />}
        {infoPosition && (
          <InfoWindow position={infoPosition}>
            <div>{infoContent}</div>
          </InfoWindow>
        )}
        {attractionMarkers.map((marker, index) => (
          <Marker
            key={index}
            position={marker.position}
            onClick={() => setSelectedAttraction(marker)}
          >
            {selectedAttraction === marker && (
              <InfoWindow
                position={marker.position}
                onCloseClick={() => setSelectedAttraction(null)}
              >
                <div>{marker.name}</div>
              </InfoWindow>
            )}
          </Marker>
        ))}
      </GoogleMap>
    </div>
  ) : (
    <></>
  );
}

export default React.memo(Maps);
