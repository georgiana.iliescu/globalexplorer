import React from "react";
import video from "../../Media/video.mp4";
import "./HomePage.css";
import Transition from "../../components/Utils/Transition";

function HomePage() {
  return (
    <div className="video-container">
      <video autoPlay loop muted className="video-background">
        <source src={video} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
      <div className="video-description">
        <h1 style={{ fontSize: "3em" }}>Bine ați venit pe pagina noastră!</h1>
        <p style={{ fontSize: "2em" }}>
          GlobalExplorer este aplicația perfectă pentru pasionații de călătorii!
          Planifică-ți următoarea aventură într-un mod simplu și captivant.
          Descoperă lumea și creează amintiri de neuitat în fiecare călătorie!
        </p>
      </div>
    </div>
  );
}

export default Transition(HomePage);
