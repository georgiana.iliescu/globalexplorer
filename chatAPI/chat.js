const express = require("express");
const OpenAi = require("openai");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());

const openai = new OpenAi({
  apiKey: "sk-proj-Ti30cMHvwZ3dJ7czAJytT3BlbkFJQRaFwpbotHqulDhNMwN9",
});

async function getOpenAiResponse(userPrompt) {
  const response = await openai.chat.completions.create({
    model: "gpt-3.5-turbo",
    messages: [{ role: "user", content: userPrompt }],
    max_tokens: 1000,
  });
  return response.choices[0].message.content;
}

app.post("/getResponse", async (req, res) => {
  const userPrompt = req.body.userPrompt;
  try {
    const aiResponse = await getOpenAiResponse(userPrompt);
    res.send(aiResponse);
  } catch (error) {
    console.error("Error fetching AI response:", error);
    res.status(500).send("An error occurred while processing your request.");
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
